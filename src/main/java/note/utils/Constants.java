package note.utils;

public class Constants {
	public static int minNota = 1;
	public static int maxNota = 10;
	public static int minNrmatricol = 1;
	public static int maxNrmatricol = 1_000_000_000;
	public static String notaNormala = "normal";
	public static String notaTeza= "teza";
	public static String invalidNota = "Nota introdusa nu este corecta";
	public static String invalidMateria = "Lungimea materiei este invalida";
	public static String invalidNrmatricol = "Numarul matricol introdus nu este corect";
	public static String invalidTip = "Tipul notei nu este unul valid. Alegeti dintre \"" + notaNormala + "\" si \"" + notaTeza + "\"";
	public static String emptyRepository = "Nu exista date";

}
