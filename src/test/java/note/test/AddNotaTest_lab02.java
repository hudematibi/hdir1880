package note.test;

import note.controller.NoteController;
import note.model.Nota;
import note.repository.NoteRepository;
import note.repository.NoteRepositoryMock;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class AddNotaTest_lab02 {

	private NoteRepository repo;

	@Before
	public void init() {
		repo = new NoteRepositoryMock();
	}

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	//BVA nrMatricol
	@Test
	public void test1() throws ClasaException {
		Nota nota = new Nota(1, "Desen", 10);
		repo.addNota(nota);
		assertEquals(1, repo.getNote().size());
	}

	@Test
	public void test2() throws ClasaException {
		Nota nota = new Nota(2, "Desen", 10);
		repo.addNota(nota);
		assertEquals(1, repo.getNote().size());
	}

	@Test
	public void test3() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(0, "Desen", 10);
		repo.addNota(nota);
	}


	@Test
	public void test4() throws ClasaException {
		Nota nota = new Nota(999999999, "Desen", 10);
		repo.addNota(nota);
		assertEquals(1, repo.getNote().size());
	}

	@Test
	public void test5() throws ClasaException {
		Nota nota = new Nota(1000000000, "Desen", 10);
		repo.addNota(nota);
		assertEquals(1, repo.getNote().size());
	}

	@Test
	public void test6() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNrmatricol);
		Nota nota = new Nota(1000000001, "Desen", 10);
		repo.addNota(nota);
	}

	@Test
	public void test7() throws ClasaException {
		Nota nota = new Nota(123456, "Desen", 10);
		repo.addNota(nota);
		assertEquals(1, repo.getNote().size());
	}

	//BVA Note
	@Test
	public void test8() throws ClasaException {
		Nota nota = new Nota(123456, "Desen", 1);
		repo.addNota(nota);
		assertEquals(1, repo.getNote().size());
	}

	@Test
	public void test9() throws ClasaException {
		Nota nota = new Nota(123456, "Desen", 2);
		repo.addNota(nota);
		assertEquals(1, repo.getNote().size());
	}

	@Test
	public void test10() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(123456, "Desen", 0);
		repo.addNota(nota);
	}

	@Test
	public void test11() throws ClasaException {
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.invalidNota);
		Nota nota = new Nota(123456, "Desen", 11);
		repo.addNota(nota);
	}

	@Test
	public void test12() throws ClasaException {
		Nota nota = new Nota(123456, "Desen", 10);
		repo.addNota(nota);
		assertEquals(1, repo.getNote().size());
	}

	@Test
	public void test13() throws ClasaException {
		Nota nota = new Nota(123456, "Desen", 9);
		repo.addNota(nota);
		assertEquals(1, repo.getNote().size());
	}

	@Test
	public void test14() throws ClasaException {
		Nota nota = new Nota(123456, "Desen", 5);
		repo.addNota(nota);
		assertEquals(1, repo.getNote().size());
	}
}