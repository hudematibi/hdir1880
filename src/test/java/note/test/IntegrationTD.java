package note.test;

import note.controller.NoteController;
import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.ClasaRepository;
import note.repository.ClasaRepositoryMock;
import note.utils.ClasaException;
import note.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class IntegrationTD {
    NoteController ctrl;
    private ClasaRepository repo;

    @Before
    public void setUp() throws Exception {
        ctrl = new NoteController();
        repo = new ClasaRepositoryMock();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testTB_UA() throws ClasaException {
        //P->A
        Nota nota1 = new Nota(1, "Engleza", 3);
        Nota nota2 = new Nota(1, "Franceza", 4);
        ctrl.addNota(nota1);
        ctrl.addNota(nota2);
        assertEquals(2, ctrl.getNote().size());
    }

    @Test
    public void testTB_IB() throws ClasaException {
        //P->A->B A-valid B-valid
        Elev e1 = new Elev(1, "Ana");
        ctrl.addElev(e1);
        Nota nota = new Nota(1, "Muzica", 8);
        ctrl.addNota(nota);
        assertEquals(1, ctrl.getNote().size());
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(1, rezultate.size());
    }

    @Test
    public void testTB_IC() throws ClasaException {
        //P->B->A->C B-valid A-valid C-valid
        Elev e1 = new Elev(1, "Ana");
        ctrl.addElev(e1);
        Nota nota1 = new Nota(1, "Matematica", 3);
        ctrl.addNota(nota1);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(1, rezultate.size());

        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        Nota nota2 = new Nota(1, "Muzica", -3);
        ctrl.addNota(nota2);

        List<Corigent> corigenti = ctrl.getCorigenti();
        assertEquals(corigenti.size(),1);

    }

}
