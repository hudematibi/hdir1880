package note.test;

import note.controller.NoteController;
import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.ClasaRepository;
import note.repository.ClasaRepositoryMock;
import note.utils.ClasaException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class IntegrationBB {

    NoteController ctrl;
    private ClasaRepository repo;

    @Before
    public void setUp() throws Exception {
        ctrl = new NoteController();
        repo = new ClasaRepositoryMock();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    @Test
    public void testBB_U1() throws ClasaException {
        Nota nota = new Nota(1, "Geogrefie", 10);
        ctrl.addNota(nota);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void testBB_U2() throws ClasaException {
        Elev e1 = new Elev(1, "Elev1");
        Nota n1 = new Nota(1,"Materie1", 10);
        Nota n2 = new Nota(1,"Materie1", 8);
        Nota n3 = new Nota(1,"Materie1", 9);
        Nota n4 = new Nota(1,"Materie2", 10);
        Nota n5 = new Nota(1,"Materie3", 8);
        ctrl.addElev(e1);
        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.addNota(n5);
        repo.creazaClasa(ctrl.getElevi(), ctrl.getNote());
        double medieElev = repo.medieElev(e1);
        assertEquals(medieElev,9,0.0001);
    }

    @Test
    public void testBB_U3() throws ClasaException {
        Elev e1 = new Elev(1, "Ana");
        Elev e2 = new Elev(2, "Ion");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota n1 = new Nota(1,"Romana", 5);
        Nota n2 = new Nota(1,"Romana", 3);
        Nota n3 = new Nota(1,"Matematica", 10);
        Nota n4 = new Nota(1,"Matematica ", 10);
        Nota n5 = new Nota(2,"Romana", 6);
        Nota n6 = new Nota(2,"Romana", 5);
        Nota n7 = new Nota(2,"Matematica", 2);
        Nota n8 = new Nota(2,"Matematica", 3);
        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.addNota(n5);
        ctrl.addNota(n6);
        ctrl.addNota(n7);
        ctrl.addNota(n8);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Corigent> corigenti = ctrl.getCorigenti();
        assertEquals(corigenti.size(),2);
    }

    @Test
    public void testBB_I1() throws ClasaException {
        //P->A->B->C A-valid B-valid C-valid
        Elev e1 = new Elev(1, "Ana");
        ctrl.addElev(e1);
        Nota nota = new Nota(1, "Muzica", 7);
        ctrl.addNota(nota);
        assertEquals(1, ctrl.getNote().size());
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        ClasaRepositoryMock repo = (ClasaRepositoryMock) ctrl.getClasaRepo();
        double medieElev = repo.medieElev(e1);

        assertEquals(1, rezultate.size());
        assertEquals(medieElev,7,0.0001);
        List<Corigent> corigenti = ctrl.getCorigenti();
        assertEquals(corigenti.size(),0);
    }
}
